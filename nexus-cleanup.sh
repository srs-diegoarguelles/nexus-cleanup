
#!/bin/bash
#Make sure you have nexus-cli already downloaded on the remote server.
#wget https://s3.eu-west-2.amazonaws.com/nexus-cli/1.0.0-beta/linux/nexus-cli
#In this case, our containing folder will be /home/ec2-user

#Execute this line to connect to nexus server instance and start cleanup process. 
#ssh -i ./polis-nexus.pem ec2-user@10.52.149.201 'bash -s' <  nexus-cleanup.sh
#Make sure the polis-nexus key address is correct for you. This command will log us into the remote instance and execute this file there.

#Making nexus-cli available in our current session
echo $PATH
PATH=$PATH:/home/ec2-user
export PATH
chmod +x nexus-cli
source .bash_profile

#Configuration: The order is -> HOST / REPOSITORY / USER / PASS
nexus-cli configure
http://127.0.0.1:81
datalabs-prod
admin
serasa@1

#Executing clean all task

#First we will show how many instances we have for each docker image
COUNTER=0;
image_info=''
for i in $(nexus-cli image ls); do
	for t in $(nexus-cli image tags -name $i); do
		# We want to get this result: There are X images for IMAGE_NAME
		if  [[ $t == There* || $t == *are*  || $t == *images*  || $t == *for*  || $t == *$i* ]]  ;
		then
			(( ++COUNTER ))
			if  (( COUNTER > 0 )) && (( COUNTER < 7 ));
			then
				image_info=$image_info" "$t
			fi;
			#If we already have one of those values, we dont want to evaluate the following conditions.
			continue;
		fi;
		#If the counter is 2, then the last word on image_info is 'are'. Next, we will find the number of instances.
		if  [[ COUNTER -eq 2 ]]; 
		then
			image_info=$image_info" "$t
			#We need to increase the counter to make sure we will get the rest of the sentence.
			(( ++COUNTER ))
		fi;
		#6 is the maximum number of words that the result will have. So, once here, we just reset all the values.
		if  [[ COUNTER -eq 6 ]];
		then
			echo $image_info
			COUNTER=0;
			image_info=''
		fi;
	done
	#nexus-cli image delete -name "$i" -keep 5 
done

#Now, the best part. We will kill the old images and let just the 2 most recent. Make sure and request authorization (if apply) to uncomment the following piece of code
for i in $(nexus-cli image ls); do
	#Here is the venom.
	#nexus-cli image delete -name "$i" -keep 2 
done



